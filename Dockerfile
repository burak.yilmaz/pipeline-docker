FROM python:3.6-slim-stretch

# Airflow
ARG AIRFLOW_VERSION=1.10.11
ARG AIRFLOW_EXTRAS="aws,ssh"

# Invent's python package repository
ARG INVENT_PYPI=https://s3-eu-west-1.amazonaws.com/pypi.inventanalytics.com

# Add missing man directories on debian
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863199
RUN mkdir -p /usr/share/man/man1

COPY requirements.txt /requirements.txt

# Install apt and pip requirements
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        build-essential \
        apt-utils \
        ssh \
        awscli \
        jq \
        git \
        unixodbc \
        unixodbc-dev\
        default-jre \
        default-jdk \
    && apt-get autoremove -yqq --purge \
    && apt-get clean \
    && pip install pip setuptools wheel --upgrade \
    # airflow with extras
    && pip install apache-airflow[${AIRFLOW_EXTRAS}]==${AIRFLOW_VERSION} \
        --constraint https://raw.githubusercontent.com/apache/airflow/${AIRFLOW_VERSION}/requirements/requirements-python3.6.txt \
    && pip install -r requirements.txt

RUN ssh-keygen -b 4096 -t rsa -f ~/.ssh/id_rsa -N '' -q \
   && cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
