#!/bin/bash

# write all environment variables to use them after ssh login
env >> /etc/environment

# ssh service
service ssh start

case "$1" in
  webserver)
    airflow initdb
    airflow variables --import ${AIRFLOW__CORE__DAGS_FOLDER}/variables.json
    airflow connections --add \
        --conn_id ssh_spider_emr \
        --conn_type ssh \
        --conn_host 'localhost' \
        --conn_port 22
    airflow scheduler &
    exec airflow webserver
    ;;
  *)
    # The command is not an airflow subcommand. Just run it.
    exec "$@"
    ;;
esac
