# Day-1-Dag Pipeline

Run the day-1-dag pipeline locally with docker for development.

## Requirements

### Environment variables

* `NOOB_DIR`
* `ROCKET_DIR`
* `ROCKS_DIR`
* `WARP_DIR`
* `ZEUS_DIR`
* `SPIDER_DIR`
* `SAMPLE_DATA_DIR`

### Spider pipeline repo

```sh
git clone git@gitlab.com:invent-analytics/pipeline/spider.git $SPIDER_DIR
```

### Invent libraries

```sh
git clone git@gitlab.com:invent-analytics/airflow/zeus.git $ZEUS_DIR
git clone git@gitlab.com:invent-analytics/sparky/noob.git $NOOB_DIR
git clone git@gitlab.com:invent-analytics/sparky/rocks.git $ROCKS_DIR
git clone git@gitlab.com:invent-analytics/data-transfer/warp.git $WARP_DIR
git clone git@gitlab.com:invent-analytics/sparky/rocket.git $ROCKET_DIR
```

### Sample data

Find the latest integration test UUID from slack notification.

```sh
aws s3 sync s3://invent-spider-datastore/integration-tests/{UUID} $SAMPLE_DATA_DIR --exclude="*" --include="*.parquet" --quiet
```

### Local Configuration

Edit `remote_host`, `file_schema` and `bucket_name` in DAG config yaml files and script config json files as follows:

* `remote_host` -> `"pipeline-docker_emr_1"`
* `file_schema` -> `"/"`
* `bucket_name` -> `"home/emr/datastore"`

## Usage

```sh
git clone git@gitlab.com:burak.yilmaz/pipeline-docker.git
cd pipeline-docker
docker-compose build
docker-compose up
```

Wait until the airflow webserver is ready. Then go to localhost:8080

## Architecture
![Pipeline Docker](pipeline-docker.png)
[gslide](https://docs.google.com/presentation/d/1Lj8i522sU4Q8qT1ZhybVwPZze0QR-jbVwI2tD4zWVT8/edit?usp=sharing)
